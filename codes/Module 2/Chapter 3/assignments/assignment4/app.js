function check() {
    //Initializing the vextices
    var t11 = document.getElementById("x1");
    var t12 = document.getElementById("y1");
    var t21 = document.getElementById("x2");
    var t22 = document.getElementById("y2");
    var t31 = document.getElementById("x3");
    var t32 = document.getElementById("y3");
    var t41 = document.getElementById("a");
    var t42 = document.getElementById("b");
    //Converting vertices into number format
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    var x = parseFloat(t41.value);
    var y = parseFloat(t42.value);
    //Finding area of each triangle
    var a = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2;
    var a1 = Math.abs(x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2;
    var a2 = Math.abs(x1 * (y - y3) + x * (y3 - y1) + x3 * (y1 - y)) / 2;
    var a3 = Math.abs(x1 * (y2 - y) + x2 * (y - y1) + x * (y1 - y2)) / 2;
    //Finding sum and equating..
    var sum = a1 + a2 + a3;
    var ans;
    console.log(a, a1, a2, a3, sum);
    if (Math.abs(a - sum) < 0.000001) {
        ans = "The Point lies Inside the Triangle";
    }
    else {
        ans = "The Point lies Outside the Triangle";
    }
    document.getElementById("here").innerHTML = "<u><b>" + ans + "</b></u>";
    //document.getElementById("here").innerHTML+="<br>values:"+a+" "+a1+" "+a2+" "+a3+" "+sum;
}
//# sourceMappingURL=app.js.map