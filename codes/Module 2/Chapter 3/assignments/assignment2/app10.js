var x = document.getElementById("x");
var y = document.getElementById("y");
var z = document.getElementById("z");
function fun() {
    var a = parseFloat(x.value);
    var b = parseFloat(y.value);
    var c = parseFloat(z.value);
    if (a != b && b != c && c != a) {
        document.getElementById("display").innerHTML = "Scaler Triangle";
    }
    else if (a == b && b == c && c == a) {
        document.getElementById("display").innerHTML = "Equilateral Triangle";
    }
    else {
        document.getElementById("display").innerHTML = "Isosceles Triangle";
    }
    if ((a * a == b * b + c * c) || (b * b == a * a + c * c) || (c * c == a * a + b * b)) {
        document.getElementById("display").innerHTML += " and Right Angle";
    }
}
//# sourceMappingURL=app10.js.map